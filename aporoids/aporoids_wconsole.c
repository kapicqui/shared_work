/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON

  aporoids.c      - main file


  (Authors and their main areas of focus)

  Quido Kapica (kapicqui)
  - ship controls
  - ship generation
  - level generation
  - asteroid and powerup generation
  - game rendering

  Jakub Šlapák (slapajak)
  - graphic tools
  - GUI's
  - asteroid eliptic shape generation

  include your name there and license for distribution.

  Remove next text:
  "Sir, yes sir!"
   ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⢴⣛⡹⡹⣄⠀⢀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡠⠐⠉⠀⠀⠈⠓⠋⣿⣾⣿⣿⣾⣶⣤⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡏⠀⠀⠀⠀⠀⠀⠀⠀⠈⠙⣿⣿⣿⣿⣿⣿⣿⣗⢦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⡾⣶⣀⣀⠤⠤⠤⣤⣤⠤⠤⣀⣸⣿⣿⣿⣿⣿⣿⣿⣷⣷⣄⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡷⡇⠳⠶⠿⠿⠿⠿⢿⣿⣿⣿⣷⣮⣟⠻⣿⣿⣿⣿⣿⣿⡿⠀⠙⠰⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡆⡇⢳⠒⠂⠠⠤⢄⡀⠸⣿⣿⠀⢀⣿⠀⠈⠛⠛⠛⠛⠫⢄⠀⠀⠀⠀⠁⠃⠤⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇⡇⢸⠀⠀⠀⠀⠀⠘⡄⣿⡿⢸⠅⢸⠀⠀⠀⠀⠀⠀⠀⠈⠑⢦⡀⠀⠀⠀⠀⠀⠉⠘⠂⠆⣄⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇⣇⠜⠒⠒⠒⠒⢒⣼⡿⠛⠛⢮⣆⢸⡆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⢆⠀⠀⠀⠀⠀⠀⠀⠀⠈⠻⢦⡄⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢾⡥⣁⣀⣀⣀⣀⣴⢛⡁⣀⡀⣄⡀⠙⠿⢧⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠳⡀⠀⠀⠀⠀⠀⠀⠀⠀⠱⡌⠳⣄⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢙⣒⣲⣶⣶⣶⣶⣶⣶⣤⣼⣿⣯⣭⣥⡼⠃⠀⠀⣀⠤⠖⠒⠒⠉⠉⢉⣹⠿⠦⢄⡀⠀⠀⠀⠀⢀⠜⠁⣠⢬⣢
⠀⠀⠀⠀⠀⠀⠀⢀⡴⢦⢀⡔⢫⠉⠉⠁⠀⠀⠈⠉⠉⠛⠛⢻⡛⠛⠋⠙⠦⢄⣠⠔⠋⠀⠀⠀⢀⣀⣤⣶⣾⡄⠀⠀⠀⠀⠉⠉⠁⢸⣧⣠⠞⠉⠀⠉
⠀⠀⠀⠀⠀⢀⠞⠁⠀⠈⢿⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠱⡄⠀⠀⠀⠀⠈⢶⣶⣶⣿⣿⣿⣿⣿⣿⣿⣷⡀⠀⠀⠀⠀⠀⣀⣨⡏⠁⠀⠀⠀⠀
⠀⠀⠀⢠⠞⠁⠀⠀⠀⠀⢸⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢷⡀⠀⠀⠀⠀⠈⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⣀⣠⠴⠖⠛⠉⠀⠀⠀⠀⠀⠀⠀
⠀⠀⢠⠏⠀⠀⠀⠀⠀⠀⢈⣿⣿⣇⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⣠⠼⢣⣄⣀⠀⠀⠀⠈⣿⣿⣿⣿⠟⠛⠛⠉⠁⠈⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⡼⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⠗⠍⡒⠤⡠⢒⡨⠝⠒⠒⠉⠁⠀⠀⠈⠐⠩⢅⡒⢄⣸⡅⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⡇⠀⠀⠀⠀⠀⠀⢀⣾⣿⠟⠀⠀⠈⠑⠔⠉⠀⠀⠀⣀⠤⠒⠒⠒⠢⠄⡀⠀⠈⢢⡽⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⣧⠀⠀⠀⠀⠀⣠⣾⣿⡋⠀⠀⠀⠀⠀⠀⠀⠀⡠⠊⠒⡄⠀⠀⠀⠀⠀⡼⢆⠀⢸⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠈⢻⠶⠶⠾⣿⣿⣿⡇⢳⡀⠀⠀⠀⠀⠀⠀⢰⠑⠢⡀⠸⡀⠀⠀⠀⡰⢁⡼⠀⣼⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⣾⠀⠀⠀⠀⠀⢉⡿⠀⠱⡄⠀⠀⠀⠀⢀⠇⠀⠀⠘⡄⡇⠀⠀⢠⢃⠎⠀⡇⡟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⢀⡇⠀⠀⠀⠀⠀⣸⠃⢀⡶⢋⠔⡲⠤⠄⠊⠀⠀⠀⠀⢱⢰⠀⠀⡌⡎⠀⠀⣇⣷⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⢸⠃⠀⠀⠀⠀⠀⡟⠀⢸⡗⡾⠀⡏⢰⠒⠒⢲⠒⠂⠠⡬⠾⠤⡤⠷⠤⡤⢄⢧⡇⢹⠆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⡾⣤⣀⣀⡀⠀⢸⠇⠀⣸⠣⡇⠀⡇⢸⠀⠀⠸⠀⠀⢸⠀⠀⡤⠇⠀⢨⠁⢸⢸⡇⢸⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⢰⠇⠿⠿⠿⣿⣿⡏⠀⠀⣿⠀⡇⠀⣇⣚⠀⠀⡆⠀⠀⢸⠀⠀⠓⡄⠀⢸⠀⢸⠸⡇⢸⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⡾⠀⠀⠀⠀⠀⠈⡇⠀⠀⠓⣶⢳⡸⠀⠀⠈⠉⠉⠉⠀⠚⠒⠒⠒⠓⠒⠚⠒⠛⢣⣇⣸⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⢰⡇⠀⠀⠀⠀⠀⠀⢿⠀⠀⠀⠸⣷⣿⣶⣤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⡏⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⢸⡄⠀⠀⠀⠀⠀⠀⣼⠀⠀⠀⢀⡏⠉⠙⠻⣿⣦⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡞⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠸⡇⠀⠀⠀⠀⠀⢰⠇⠀⠀⠀⣸⠁⠀⠀⠀⠈⢻⣿⣷⡄⠀⡰⠁⠀⠀⠸⡀⢠⣾⣷⢸⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
 *******************************************************************/

#define _POSIX_C_SOURCE 200112L
#define _MENU 1
#define _PLAYING 2
#define _PAUSED 3
#define _GAMEOVER 66
#define SPILED_REG_PRESS_KNOBS_8BIT_o 0x029
#define WIDTH 480
#define HEIGHT 320
#define SHIP_VALUE 0xF800
#define CANNON_VALUE 0x07FF
#define LASER_VALUE 0x3666
#define ASTEROID_VALUE_ONE 0x5000
#define ASTEROID_VALUE_TWO 0xBDEF
#define ASTEROID_VALUE_THREE 0x7777
#define HIT_VALUE 0xFFE0
#define SHIELD_VALUE 0xF154


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <string.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"

#include "font_types.h"


// Structure to keep track of controls
typedef struct knob_t{
  // Extremes/Turning points for the knobs, to be able for them to function for just the 80 values of a knob
  int prev_ex, curr_ex, next_ex;
  // Current value of knob
  uint8_t knob_value;
  // Value of approximately -80 to 80 for the actual controls (knob_value - curr_ex)
  int control_value;
} knob_t;

// object structure
// object field size is set to final of 61x61
typedef struct object{
  // movement data
  int direction;
  float speed;
  // coordinates
  float coord_x; 
  float coord_y;
  unsigned short texture[61][61]; // odd Y anb X axis allows for common center and rotation of objects
  // for objects in the asteroid field (asteroids and powerups)
  struct object *prev_node;
  struct object *next_node;
} object_t;


// ship structure
typedef struct ship_t{
  // shooting functions controls
  int gun_direction;
  int shot_delay;
  int shield;
  int hit;
  // sub-models for ship hull and cannon
  object_t *ship_model;
  // cannon is basically prepared next laser shot
  object_t *cannon;
} ship_t;

// object queue structure for asteroid field
typedef struct object_list{
  int size;
  object_t *first_object;
  object_t *last_object;
} object_list;








// keeps track of when to turn off
bool game_running = true;
// Initital game state
int game_state = _MENU;
unsigned char *mem_base;
unsigned char *parlcd_mem_base;
// Height = 320 px & Width = 480 px
unsigned short field[HEIGHT][WIDTH];


// define positions for storing information
// controls
uint32_t rgb_knobs_value;
uint8_t buttons;
knob_t *red_knob, *green_knob, *blue_knob;
// ship and laser info
ship_t *ship;
object_t *laser;
uint32_t ship_hp;
int score;
// asteroid field info
object_list *asteroid_field;

// Int values for cycles
int i,j,x,y;
// control functions to keep track of amount of allocated memory
int allocd;
int freed;

// pointers and variables for LCD display manipulation
int ptr;
unsigned short c;  
unsigned short *fb;
unsigned short *fbackground;
font_descriptor_t* font;
unsigned short color = 0x0000;








/* ==== INITIALIZATION SECTION ====
*object_t *initialize_object_t()
*ship_t *initialize_ship_t()
*knob_t *initialize_knob()
*/


// initialization of objects
object_t *initialize_object_t() {
    object_t *new_object = (object_t *)malloc(sizeof(object_t));
    if (new_object != NULL) {
        //allocd++;
        // Initialize all variables
        new_object->direction = 0;
        new_object->speed = 0.0f;
        new_object->coord_x = 0.0f;
        new_object->coord_y = 0.0f;
        // Initialize texture field
        for (i = 0; i < 61; ++i) {
            for (j = 0; j < 61; ++j) {
                new_object->texture[i][j] = 0;
            }
        }
        // Makes sure that pointers are NULL (in case they won't be used)
        new_object->prev_node = NULL;
        new_object->next_node = NULL;
    }
    return new_object;
}

// ship initialization
ship_t *initialize_ship_t() {
    ship_t *new_ship = (ship_t *)malloc(sizeof(ship_t));
    if (new_ship != NULL) {
        //allocd++;
        // Inicializovat všechny členy struktury ship_t
        new_ship->gun_direction = 0;
        new_ship->shot_delay = 0;
        new_ship->shield = 0;
        new_ship->hit = 0;
        // Pokud jsou členy ukazatelů, mohou být inicializovány na NULL
        new_ship->ship_model = initialize_object_t();
        new_ship->cannon = NULL;
    }
    return new_ship;
}


// Initialize knob_t
knob_t *initialize_knob() {
  knob_t *knob = (knob_t *)malloc(sizeof(knob_t));
  if (knob != NULL) {
    //allocd++;
    knob->prev_ex = 0;
    knob->curr_ex = 0;
    knob->next_ex = 0;
    knob->knob_value = 0;
    knob->control_value = 0;
  }
  return knob;
}





/* ==== GRAPHIC TOOLS SECTION ====
* void draw_pixel(int x, int y, unsigned short color)
* void print_color(int x, int y, unsigned short color)
* void draw_background(unsigned short *fb, int x, int y, int width, int height, unsigned short color)
* const font_bits_t* get_char_bitmap(const font_descriptor_t* font, char c)
* void draw_character(const font_descriptor_t* font, unsigned short *fb, char c, int x, int y, unsigned short color)
* int char_width(const font_descriptor_t* font, int ch)
* void string_printing(char text[],int offset, int width, font_descriptor_t* font, unsigned short color, int x, int y)
* void draw_circle(int centerX, int centerY, int radius, unsigned short color)
* void draw_pattern(int x, int y, int width, int height, unsigned short color)
*/


void draw_pixel(int x, int y, unsigned short color) {
    if (x >= 0 && x < 480 && y >= 0 && y < 320) {
        fb[x + 480 * y] = color;
    }
}


void print_color(int x, int y, unsigned short color) {
    if (x >= 0 && x < 480 && y >= 0 && y < 320) {
        fbackground[x + 480 * y] = color;
    }
}


void draw_background(unsigned short *fb, int x, int y, int width, int height, unsigned short color) {
    for (int i = y; i < y + height; i++) {
        for (int j = x; j < x + width; j++) {
            if (j >= 0 && j < 480 && i >= 0 && i < 320) {
                print_color(j, i, color);
            }
        }
    }
}


const font_bits_t* get_char_bitmap(const font_descriptor_t* font, char c) {
    // Spočítejte index znaku v rámci fontu
    int index = c*16 - 512;
    
    // Zkontrolujte, zda index není mimo rozsah bitmap v fontu
    if (index >= 0 && index < font->size) {
        // Pokud index odpovídá platnému znaku, vrátíme jeho bitmapu
        return &font->bits[index];
    } else {
        // Pokud znak není v rozsahu fontu, vrátíme NULL
        return NULL;
    }
}


void draw_character(const font_descriptor_t* font, unsigned short *fb, char c, int x, int y, unsigned short color) {
    const font_bits_t *bitmap = get_char_bitmap(font, c);

    if (!bitmap) return;

    for (int i = 0; i < font->height; i++) {
        uint16_t row_data = bitmap[i];
        for (int j = 0; j < font->maxwidth; j++) {
            if ((row_data >> (15 - j)) & 0x01) {
                draw_pixel(x + j, y + i, color);
            }
        }
    }
}


int char_width(const font_descriptor_t* font, int ch) {
    if ((ch >= font->firstchar) && (ch - font->firstchar < font->size)) {
        ch -= font->firstchar;
        if (!font->width) {
            return font->maxwidth;
        } else {
            return font->width[ch];
        }
    }
    return 0;
}


void string_printing(char text[],int offset, int width, font_descriptor_t* font, unsigned short color, int x, int y){
  for (int i = 0; i < strlen(text); i++) {
        if (offset + width + 16 <= 480 - 220) {
            draw_character(font, fb, text[i], x + offset, y, color);
            width = char_width(font, text[i]);
            offset +=5 + width;
        } else {
            break;
        }
    }
  printf("offset %d\n", offset); // to test if test in middle 
}


void draw_circle(int centerX, int centerY, int radius, unsigned short color) {
    int x, y;

    // Vykreslení kruhu
    for (y = 0; y < 360; y++) {
        for (x = 0; x < 480; x++) {
            int dx = x - centerX;
            int dy = y - centerY;
            if (dx*dx + dy*dy <= radius*radius) {
                draw_pixel(x, y, color); // Pokud je bod uvnitř kruhu, vykreslíme pixel
            }
        }
    }
}


void draw_pattern(int x, int y, int width, int height, unsigned short color) {
    int i, j;

    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            draw_pixel(x + j, y + i, color);
        }
    }
}




















/* ==== GUI's SECTION ====
*void draw_introduction() 
*void draw_menu()
*void draw_instructions()
*void draw_game()
*void draw_paused()
*void draw_gameover()
*void draw_bye()
*/



void draw_introduction() {
    draw_background(fbackground, 0, 0, 480, 360, 0xBDEF);
    memcpy(fb, fbackground, 320 * 480 * 2);
    sleep(2);

    color = 0x0000;
    string_printing("---APOROIDS GAME---", 0, 0, font, color,124,107);
    string_printing("Setting up game", 0, 0, font, color,150,167);
    string_printing("please wait", 0, 0, font, color,175,187);

    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
}

void draw_menu() {

    draw_background(fbackground, 0, 0, 480, 360, 0x0000);
    memcpy(fb, fbackground, 320 * 480 * 2);
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }

    color = 0x07FF;
    draw_background(fbackground, 142, 100, 196, 30, color);
    memcpy(fb, fbackground, 320 * 480 * 2);
    color = 0xF800;
    draw_background(fbackground, 142, 200, 196, 30, color);
    memcpy(fb, fbackground, 320 * 480 * 2);

    color = 0x0000;
    string_printing("START GAME", 0, 0, font, color,172,107);
    string_printing("EXIT", 0, 0, font, color,215,207);

    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
}

void draw_instructions() {

    // printing text to set knobs
    
    draw_background(fbackground, 0, 0, 480, 360, 0x0000);
    memcpy(fb, fbackground, 320 * 480 * 2);
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }

    color = 0x07FF;
      
    string_printing("Set up knobs to this", 0, 0, font, color,126,87);
    string_printing("position", 0, 0, font, color,194,107);

    // red knob
    draw_circle(150, 200, 30,0xF800);
    draw_pattern(147, 160, 5, 20, 0xF800);


    //gren knob
    draw_circle(240, 200, 30,0x3666);
    draw_pattern(237, 160, 5, 20, 0x3666);


    //blue knob
    draw_circle(330, 200, 30,0x07FF);
    draw_pattern(327, 160, 5, 20, 0x07FF);


    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }

    for (i = 0; i < 5; i++) {
      char time_str[4];
      int time = 5 - i;
      snprintf(time_str, sizeof(time_str), "%d", time);
      string_printing(time_str, 0, 0, font, color, 240, 55);
      parlcd_write_cmd(parlcd_mem_base, 0x2c);
      for (ptr = 0; ptr < 480 * 320; ptr++) {
          parlcd_write_data(parlcd_mem_base, fb[ptr]);
      }
      string_printing(time_str, 0, 0, font, 0x0000, 240, 55);
      sleep(1);
    }

}


void draw_game() {
      printf("drawing the game\n");
  ptr = 0;
  if (parlcd_mem_base != NULL) {
    printf("?\n");
    i = 0;
    j = 0;
    for (i = 0; i < 320 ; i++) {
        printf("%d ", i);
      for (j = 0; j < 480 ; j++) {
        //unsigned int c = field[i][j];
        c = field[i][j];
        fb[ptr]=c;
        ptr++;
        //parlcd_write_data(parlcd_mem_base, fb[ptr]);
      }
        printf("-k\n");
    }

    if (score > 999) {
      score = 0;
    }
    color = 0xE6D8;
    string_printing("Score: ", 0, 0, font, color, 160, 15);
    char score_str[4];
    sprintf(score_str, "%d", score);
    string_printing(score_str, 0, 0, font, color, 240, 15);
    string_printing("Pause: ", 0, 0, font, color, 360, 15);
    color = 0x07FF;
    draw_circle(460, 23, 8,color);



    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
  }
    printf("finished drawing the game\n");
}


void draw_paused() {
    color = 0x07FF;
    draw_pattern(155, 200, 170, 30, color);

    color = 0xF800;
    string_printing("--- GAME PAUSED ---", 0, 0, font, color, 128, 150);
    color = 0x0000;
    string_printing("CONTINUE", 0, 0, font, color, 185, 207);


    color = 0xF800;
    draw_pattern(155, 240, 170, 30, color);
    color = 0x0000;
    string_printing("EXIT", 0, 0, font, color,215,247);

    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
}


void draw_gameover() {
    color = 0xF800;
    string_printing("--- GAME OVER ---", 0, 0, font, color, 141, 100);
    string_printing("Player final", 0, 0, font, color, 173, 150);

    int final_score = score;
    int string_x = 0;
    int score_x = 0;
    if (final_score < 10) {
      string_x = 198;
      score_x = 268;
    } else if(final_score >=10 && final_score < 100){
      string_x = 192;
      score_x = 262;
    } else if (final_score >= 100 && final_score < 1000){
      string_x = 185;
      score_x = 255;
    }else{
      string_x = 180;
      score_x = 250;
    }

    string_printing("score:", 0, 0, font, color, string_x, 180);
    char score_str[4];
    sprintf(score_str, "%d", final_score);
    string_printing(score_str, 0, 0, font, color, score_x, 180);


    color = 0xF800;
    draw_pattern(155, 240, 170, 30, color);
    color = 0x0000;
    string_printing("EXIT", 0, 0, font, color,215,247);

    
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
      for (ptr = 0; ptr < 480 * 320; ptr++) {
          parlcd_write_data(parlcd_mem_base, fb[ptr]);
      }
}


void draw_bye() {
    // printing byeeeee
    
    draw_background(fbackground, 0, 0, 480, 360, 0x0000);
    memcpy(fb, fbackground, 320 * 480 * 2);
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }

    color = 0x07FF;
    string_printing("BYEEEEE", 0, 0, font, color,126,87);


    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
}




/*
* GAME FIELD SETTER
*/

void clear_field() {
  for (i = 0; i < 320 ; i++) {
    for (j = 0; j < 480 ; j++) {
      field[i][j] = 0x0000;
    }
  }
}



/* ==== OBSTACLES AND POWERUP CONTROL SECTION ====
*void initialize_asteroids() 
*void generate_asteroid()
*void generate_powerup()
*void generate_wave()
*void delete_asteroid(object_t *asteroid)
*void update_asteroids()
*void print_asteroids()
*/

void initialize_asteroids() {
  asteroid_field = (object_list *)malloc(sizeof(object_list));
  //allocd++;
  asteroid_field->size = 0;
  asteroid_field->first_object = NULL;
  asteroid_field->last_object = NULL;
}

void generate_asteroid() {
  object_t *asteroid = (object_t *)malloc(sizeof(object_t));
  allocd++;
printf("HERE 2\n");
int counter = 0;
  int center = 30;
  int noise;
  // equasion for random number between 10 and 30 (rand() % (max - min + 1)) + min;
  int height_half = (rand() % (20 + 1)) + 10;
  int width_half = (rand() % (20 + 1)) + 10;
  if (height_half > width_half) {
    width_half = height_half;
  }
  printf("HERE 3\n");
  for (y = 0; y < 61; y++) {
    for (x = 0; x < 61; x++) {
      asteroid->texture[y][x] = 0;
    }
  }

printf("HERE 4\n");
  for (y = 0; y < (height_half*2+1); y++) {
    for (x = 0; x < (width_half*2+1); x++) {
      if ((x-center) * (x-center) * height_half * height_half + 
      (y-center) * (y-center) * width_half * width_half < 
      height_half * height_half * width_half * width_half) {
        noise = (rand() + score) % 4;
        if (noise != 0) {
          noise = (rand() + score) % 2;
          if (noise == 0) {
            asteroid->texture[y][x] = ASTEROID_VALUE_ONE;
            counter++;
          }
          else {
            asteroid->texture[y][x] = ASTEROID_VALUE_TWO;
            counter++;
          }
        }
      }
    }
  }
  if (counter < 400) {
    for (y = 20; y < 31; y++) {
      for (x = 20; x < 31; x++) {
        if (abs(30-x) + abs(30-y) < 12) {
            noise = (rand() + score) % 2;
            if (noise == 0) {
              asteroid->texture[y][x] = ASTEROID_VALUE_ONE;
            }
            else {
              asteroid->texture[y][x] = ASTEROID_VALUE_TWO;
            }
        }
      }
    }
  }

printf("HERE 5\n");
  if (asteroid_field->first_object == NULL || asteroid_field->size < 1) {
    asteroid_field->first_object = asteroid;
    asteroid_field->last_object = asteroid;
    asteroid->prev_node = NULL;
    asteroid->next_node = NULL;
  }
  else {
    asteroid_field->last_object->next_node = asteroid;
    asteroid->prev_node = asteroid_field->last_object;
    asteroid->next_node = NULL;
    asteroid_field->last_object = asteroid;
  }
    asteroid_field->size++;

printf("HERE 6\n");
  noise = rand() % 2;
  if (noise == 0) {
    noise = rand() % 2;
    if (noise == 0) {
      asteroid->coord_x = 0;
    }
    else {
      asteroid->coord_x = 479;
    }
    asteroid->coord_y = rand() % 320;
  }
  else {
    noise = rand() % 2;
    if (noise == 0) {
      asteroid->coord_y = 0;
    }
    else {
      asteroid->coord_y = 319;
    }
    asteroid->coord_x = rand() % 480;
  }
  
  printf("HERE 7\n");
  asteroid->speed = 4 + (rand() % 9);
  asteroid->direction = (rand() % 80);
}


void generate_powerup() {
  object_t *powerup = (object_t *)malloc(sizeof(object_t));
  allocd++;
  printf("HERE 3\n");
  for (y = 0; y < 61; y++) {
    for (x = 0; x < 61; x++) {
      powerup->texture[y][x] = 0;
    }
  }

printf("HERE 4\n");
  for (y = 0; y < 61; y++) {
    for (x = 15; x < 46; x++) {
      if (y > 14 && y < 31 && x > 14 && x < 46) {
        powerup->texture[y][x] = SHIELD_VALUE;
      }
      if (y > 30 && x > (y-30)*2 && x < 61-(y-30)*2) {
        powerup->texture[y][x] = SHIELD_VALUE;
      }
    }
  }

printf("HERE 5\n");
  powerup->speed = 0;
  if (asteroid_field->first_object == NULL || asteroid_field->size < 1) {
    asteroid_field->first_object = powerup;
    asteroid_field->last_object = powerup;
    powerup->prev_node = NULL;
    powerup->next_node = NULL;
  }
  else {
    asteroid_field->last_object->next_node = powerup;
    powerup->prev_node = asteroid_field->last_object;
    powerup->next_node = NULL;
    asteroid_field->last_object = powerup;
  }
    asteroid_field->size++;

printf("HERE 6\n");
    powerup->coord_y = rand() % 220 + 50;
    powerup->coord_x = rand() % 380 + 50;
  
  
  printf("HERE 7\n");
  powerup->direction = 0;
}


void generate_wave() {
      printf("HERE 1\n");
  int amount = rand() % (6 + (score / 10) % 11) + 2;
  for (i = 0; i < amount; i++) {
    generate_asteroid();
  }
  if ((rand() + score) % 3 == 0) {
    generate_powerup();
  }
}


void delete_asteroid(object_t *asteroid) {
  if (asteroid != NULL) {
    if (asteroid == asteroid_field->first_object) {
      asteroid_field->first_object = asteroid->next_node;
    }
    if (asteroid == asteroid_field->last_object) {
      asteroid_field->last_object = asteroid->prev_node;
    }
    if (asteroid->prev_node != NULL) {
      asteroid->prev_node->next_node = asteroid->next_node;
    }
    if (asteroid->next_node != NULL) {
      asteroid->next_node->prev_node = asteroid->prev_node;
    }
    asteroid_field->size--;
    free(asteroid);
    freed++;
  }
}


void update_asteroids() {
  printf("HERE 8\n");
  

  if (asteroid_field != NULL && asteroid_field->size == 0) {
      generate_wave();
      sleep(0.2);
    
  }
  else {
    object_t *current_asteroid = NULL;
    printf("1\n");
    if (asteroid_field != NULL) {
    printf("2\n");
      if (asteroid_field->first_object != NULL) {
    printf("3\\n");
        current_asteroid = asteroid_field->first_object;
      }
      else {
      }
    }
    printf("HERE 8B\n");
    
    if (current_asteroid != NULL && asteroid_field->size > 0) {
      printf("making asteroids\n");
      while (current_asteroid != NULL) {
        float current_asteroid_angle = 4.5 * current_asteroid->direction;

      printf("HERE 9\n");
        current_asteroid->coord_x += round(sin(current_asteroid_angle * 3.1415 / 180) * current_asteroid->speed);
        if (current_asteroid->coord_x < 0) {
          current_asteroid->coord_x += 480;
        }
        else if (current_asteroid->coord_x >= 480) {
          current_asteroid->coord_x -= 480;
        }
        current_asteroid->coord_y -= round(cos(current_asteroid_angle * 3.1415 / 180) * current_asteroid->speed);
        if (current_asteroid->coord_y < 0) {
          current_asteroid->coord_y += 320;
        }
        else if (current_asteroid->coord_y >= 320) {
          current_asteroid->coord_y -= 320;
        }
        if (current_asteroid->next_node == NULL) {
          printf("Last asteroid\n");
          printf("Last asteroid x %d\n", (int)current_asteroid->coord_x);
          printf("Last asteroid y %d\n", (int)current_asteroid->coord_y);
        }
        current_asteroid = current_asteroid->next_node;
      }
      current_asteroid = NULL;
    }
  }
  printf("HERE 10\n");
}


void print_asteroids() {
  printf("ASTEROIDS PRINTING\n");
  object_t *current_asteroid;
  bool damage;
  bool hit;
  int counter;
  int max_deletes = asteroid_field->size;
  int deletes = 0;
  current_asteroid = asteroid_field->first_object;
  while (current_asteroid != NULL) {
    damage = false;
    hit = false;
    counter = 0;
    float current_asteroid_angle = 4.5 * current_asteroid->direction;
    for (y = 0; y < 61; y++) {
      for (x = 0; x < 61; x++) {
        if (current_asteroid->texture[y][x] != 0) {
          counter++;
          int target_x = round(current_asteroid->coord_x + (x - 30) * cos(current_asteroid_angle * 3.1415 / 180) - (y - 30) * sin(current_asteroid_angle * 3.1415 / 180));
          if (target_x < 0) {
            target_x += 480;
          }
          else if (target_x >= 480) {
            target_x -= 480;
          }
          int target_y = round(current_asteroid->coord_y + (x - 30) * sin(current_asteroid_angle * 3.1415 / 180) + (y - 30) * cos(current_asteroid_angle * 3.1415 / 180));
          if (target_y < 0) {
            target_y += 320;
          }
          else if (target_y >= 320) {
            target_y -= 320;
          }



          if (field[target_y][target_x] == SHIELD_VALUE && !damage && !hit) {
            if (current_asteroid->texture[y][x] == SHIELD_VALUE) {
              ship->shield = 40;
            }
            else {
              ship->shield = 0;
            }
            hit = true;
          }
          if (field[target_y][target_x] == LASER_VALUE && !damage && !hit) {
            if (current_asteroid->texture[y][x] == SHIELD_VALUE) {

            }
            else {
              score++;
              hit = true;
            }
          }
          if (field[target_y][target_x] == SHIP_VALUE && !damage && !hit) {
            if (current_asteroid->texture[y][x] == SHIELD_VALUE) {
              ship->shield = 40;
            }
            else {
              ship->hit = 4;
              ship_hp = ship_hp >> 4;
            }
            damage = true;
          }
          if (hit || damage) {
            field[target_y][target_x] = HIT_VALUE;
          }
          else {
            field[target_y][target_x] = current_asteroid->texture[y][x];
          }
          
        }
      }
    }

    printf("test int %10f for ship\n", current_asteroid->coord_x);
    printf("test int %10f for ship\n", current_asteroid->coord_y);
    object_t *tmp_asteroid;
    tmp_asteroid = current_asteroid;
    current_asteroid = current_asteroid->next_node;
    if (hit || damage) {
      float tmp_asteroid_asteroid_angle = 4.5 * tmp_asteroid->direction;
      for (y = 0; y < 61; y++) {
        for (x = 0; x < 61; x++) {
          if (tmp_asteroid->texture[y][x] != 0) {
            int target_x = round(tmp_asteroid->coord_x + (x - 30) * cos(tmp_asteroid_asteroid_angle * 3.1415 / 180) - (y - 30) * sin(tmp_asteroid_asteroid_angle * 3.1415 / 180));
            if (target_x < 0) {
              target_x += 480;
            }
            else if (target_x >= 480) {
              target_x -= 480;
            }
            int target_y = round(tmp_asteroid->coord_y + (x - 30) * sin(tmp_asteroid_asteroid_angle * 3.1415 / 180) + (y - 30) * cos(tmp_asteroid_asteroid_angle * 3.1415 / 180));
            if (target_y < 0) {
              target_y += 320;
            }
            else if (target_y >= 320) {
              target_y -= 320;
            }

            field[target_y][target_x] = HIT_VALUE;
          }
        }
      }
      *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = ship_hp;
      if (tmp_asteroid != NULL && deletes < max_deletes) {
        delete_asteroid(tmp_asteroid);
        deletes++;
      }
    }
  }
  printf("ASTEROIDS PRINTED\n");
}

void free_asteroid_field() {
  object_t *current_asteroid = asteroid_field->first_object;
  while(current_asteroid != NULL) {
  printf("Deleting leftover asteroids\n");
    object_t *tmp_asteroid = current_asteroid->next_node;
    free(current_asteroid);
    current_asteroid = tmp_asteroid;
  }
  if(asteroid_field != NULL) {
    free(asteroid_field);
  }
}











/* ==== SHIP CONTROL FUNCTIONS ====
*
*
*
*
*/

// Resets the ship
void generate_ship() {
  for (y = 0; y < 61; y++) {
    for (x = 0; x < 61; x++) {
        ship->ship_model->texture[y][x] = 0;
    }
  }

  for (y = 60; y >= 0; y--) {
    for (x = (60-y)/2; x < y/2; x++) {
        ship->ship_model->texture[y-15][x+15] = SHIP_VALUE;
    }
  }


  for (y = 0; y < 5; y++) {
    for (x = 0; x < 5; x++) {
        ship->ship_model->texture[27+y][27+x] = CANNON_VALUE;
    }
  }

  ship->ship_model->coord_x = 240;
  ship->ship_model->coord_y = 160;
  ship->ship_model->speed = 0;
  ship->ship_model->direction = 0;
  ship->gun_direction = 0;
  ship->shot_delay = 0;
  ship->hit = 0;
}

// Generates a new laser blast 
void reload() {
  ship->cannon = initialize_object_t();

  for (y = 0; y < 61; y++) {
    for (x = 0; x < 61; x++) {
        ship->cannon->texture[y][x] = 0;
    }
  }

  for (y = 0; y < 21; y++) {
    for (x = 0; x < 7; x++) {
        ship->cannon->texture[12+y][26+x] = CANNON_VALUE;
    }
  }
  ship->cannon->speed = 14;
  ship->cannon->coord_x = ship->ship_model->coord_x;
  ship->cannon->coord_y = ship->ship_model->coord_y;
  ship->cannon->direction = ship->gun_direction;
  ship->cannon->next_node = NULL;
  ship->cannon->prev_node = NULL;
}

// Update ship based on values from controls
void update_ship() {
  ship->ship_model->direction = red_knob->control_value;
  float ship_angle = 4.5 * ship->ship_model->direction;
  ship->ship_model->speed = 2 + green_knob->control_value / 20;

  ship->ship_model->coord_x += round(sin(ship_angle * 3.1415 / 180) * ship->ship_model->speed);
  if (ship->ship_model->coord_x < 0) {
    ship->ship_model->coord_x += 480;
  }
  else if (ship->ship_model->coord_x >= 480) {
    ship->ship_model->coord_x -= 480;
  }
  ship->ship_model->coord_y -= round(cos(ship_angle * 3.1415 / 180) * ship->ship_model->speed);
  if (ship->ship_model->coord_y < 0) {
    ship->ship_model->coord_y += 320;
  }
  else if (ship->ship_model->coord_y >= 320) {
    ship->ship_model->coord_y -= 320;
  }
    printf("test int %10f for ship\n", cos(ship_angle));
    printf("test int %10f for ship\n", ship_angle);

  ship->gun_direction = (blue_knob->control_value + ship->ship_model->direction) % 80;
  if (ship->cannon != NULL) {
    ship->cannon->coord_x = ship->ship_model->coord_x;
    ship->cannon->coord_y = ship->ship_model->coord_y;
    ship->cannon->direction = ship->gun_direction;
  }

  if (ship->shield > 0) {
    ship->shield--;
  }

  ship->shot_delay+=1;
  if (ship->shot_delay == 6) {
    reload();
  }
  if (ship->shot_delay == 14) {
    free(laser);
    //freed++;
    laser = ship->cannon;
    ship->cannon = NULL;
    ship->shot_delay = 0;
  }

  if (ship->shield > 0) {
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0xFF0727;
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0xFF0727;
  }
  else if (ship->hit > 0) {
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0xFF0401;
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0xFF0401;
    ship->hit--;
  }
  else {
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0x07FF07;
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0x07FF07;
  }
}

void print_ship() {
  float ship_angle = 4.5 * ship->ship_model->direction;
  for (y = 0; y < 61; y++) {
    for (x = 0; x < 61; x++) {
      if (ship->ship_model->texture[y][x] != 0) {
        int target_x = round(ship->ship_model->coord_x + (x - 30) * cos(ship_angle * 3.1415 / 180) - (y - 30) * sin(ship_angle * 3.1415 / 180));
        if (target_x < 0) {
          target_x += 480;
        }
        else if (target_x >= 480) {
          target_x -= 480;
        }
        int target_y = round(ship->ship_model->coord_y + (x - 30) * sin(ship_angle * 3.1415 / 180) + (y - 30) * cos(ship_angle * 3.1415 / 180));
        if (target_y < 0) {
          target_y += 320;
        }
        else if (target_y >= 320) {
          target_y -= 320;
        }

        field[target_y][target_x] = ship->ship_model->texture[y][x];
      }
    }
  }

  if (ship->shield > 0) {
    for (y = -30; y < 30; y++) {
      for (x = -30; x < 30; x++) {
        if (sqrt(y * y + x * x) <= 27 && sqrt(y * y + x * x) >= 24) {
          int target_x = ship->ship_model->coord_x + x;
          if (target_x < 0) {
            target_x += 480;
          }
          else if (target_x >= 480) {
            target_x -= 480;
          }
          int target_y = ship->ship_model->coord_y + y;
          if (target_y < 0) {
            target_y += 320;
          }
          else if (target_y >= 320) {
            target_y -= 320;
          }
          field[target_y][target_x] = 0;
          field[target_y][target_x] = SHIELD_VALUE;
        }
      }
    }
  }

  if (ship->cannon != NULL) {
    for (y = 0; y < 61; y++) {
      for (x = 0; x < 61; x++) {
        float cannon_angle = 4.5 * ship->cannon->direction;
        if (ship->cannon->texture[y][x] != 0) {
          int target_x = round(ship->cannon->coord_x + (x - 30) * cos(cannon_angle * 3.1415 / 180) - (y - 30) * sin(cannon_angle * 3.1415 / 180));
          if (target_x < 0) {
            target_x += 480;
          }
          else if (target_x >= 480) {
            target_x -= 480;
          }
          int target_y = round(ship->cannon->coord_y + (x - 30) * sin(cannon_angle * 3.1415 / 180) + (y - 30) * cos(cannon_angle * 3.1415 / 180));
          if (target_y < 0) {
            target_y += 320;
          }
          else if (target_y >= 320) {
            target_y -= 320;
          }
          field[target_y][target_x] = 0;
          field[target_y][target_x] = ship->cannon->texture[y][x];
        }
      }
    }
  }
}



void update_laser() {
  if (laser != NULL) {
    float laser_angle = 4.5 * laser->direction;

    laser->coord_x += sin(laser_angle * 3.1415 / 180) * laser->speed;
    if (laser->coord_x < 0) {
      laser->coord_x += 480;
    }
    else if (laser->coord_x >= 480) {
      laser->coord_x -= 480;
    }
    laser->coord_y -= cos(laser_angle * 3.1415 / 180) * laser->speed;
    if (laser->coord_y < 0) {
      laser->coord_y += 320;
    }
    else if (laser->coord_y >= 320) {
      laser->coord_y -= 320;
    }
  }
}

void print_laser() {
  if (laser != NULL) {
    float laser_angle = 4.5 * laser->direction;
    for (y = 0; y < 61; y++) {
      for (x = 0; x < 61; x++) {
        if (laser->texture[y][x] != 0) {
          int target_x = round(laser->coord_x + (x - 30) * cos(laser_angle * 3.1415 / 180) - (y - 30) * sin(laser_angle * 3.1415 / 180));
          if (target_x < 0) {
            target_x += 480;
          }
          else if (target_x >= 480) {
            target_x -= 480;
          }
          int target_y = round(laser->coord_y + (x - 30) * sin(laser_angle * 3.1415 / 180) + (y - 30) * cos(laser_angle * 3.1415 / 180));
          if (target_y < 0) {
            target_y += 320;
          }
          else if (target_y >= 320) {
            target_y -= 320;
          }
          printf("y %d ",target_y);
          printf("a x %d\n", target_x);

          field[target_y][target_x] = LASER_VALUE;
        }
      }
    }
  }
}



void game_tick() {
  // clear previous data
  clear_field();
  // update ship
  update_ship();
  // print ship
  print_ship();
  // update laser
  update_laser();
  // print laser
  print_laser();
  // update asteroids
  update_asteroids();
  // print asteroids
  print_asteroids();
}




/* ==== CONTROLS SECTION ====
*void update_exes(knob_t *knob)
*void update_knob(knob_t *knob, uint8_t bit_val)
*void read_controls()
*void set_knob(knob_t *knob, uint8_t bit_val)
*void initialize_controls() 
*void print_knobs()
*bool check_red()
*bool check_blue()
*/

// updates extremes for knob
void update_exes(knob_t *knob) {
  if (knob != NULL && knob->curr_ex >= 0 && knob->curr_ex <= 255) {
    knob->prev_ex = knob->curr_ex - 80;
    if (knob->prev_ex < 0) {
      knob->prev_ex = knob->prev_ex + 256;
    }
    knob->next_ex = knob->curr_ex + 80;
    if (knob->next_ex > 255) {
      knob->next_ex = knob->next_ex - 256;
    }
  }
}

// updates the knob based on given values from another function
void update_knob(knob_t *knob, uint8_t bit_val){
  uint8_t int_val;
  
  if (knob != NULL) {
    int_val = bit_val;
        printf("1\n");
    knob->knob_value = int_val;

        printf("2\n");
      int tmp;
      tmp = knob->knob_value - knob->curr_ex;
        printf("3\n");
      if (tmp < 0) {
        tmp += 256;
      }
      printf("test tmp value %10d for controls\n", tmp);
      printf("prev ex %5d | curr ex %5d | next ex %5d\n", knob->prev_ex, knob->curr_ex, knob->next_ex);
        printf("checking for update of exes\n");
      // Turning points 176 (-80) | 0 | 80
      // if current calculated value is higher than 80
      if (tmp >= 80 && tmp < 176) {
        knob->curr_ex = knob->next_ex;
        printf("update of exes 1\n");

      }
      // if current calculated value is "lower than 0"
      else if (tmp > 176) {
        knob->curr_ex = knob->prev_ex;
        printf("update of exes 2\n");
      }
      knob->control_value = tmp;
      update_exes(knob);
      printf("test int %10d for controls\n", knob->control_value);
      printf("test int %10d for knob\n", knob->knob_value);
  }
}

// updates values of knobs and buttons on the device and sets the knobs accordingly
void read_controls() {
  // read knobs
  printf("W\n");
  //rgb_knobs_value = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    // store button states
    printf("X\n");
    uint8_t button_val = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 0x03);
    buttons = button_val;
    printf("Y\n");
    // update separate knob values
      printf("BLUE knob\n");
    uint8_t bit_val1 = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    update_knob(blue_knob, bit_val1);
      printf("GREEN knob\n");
    uint8_t bit_val2 = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 0x001);
    update_knob(green_knob, bit_val2);
      printf("RED knob\n");
    uint8_t bit_val3 = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 0x002);
    update_knob(red_knob, bit_val3);
  
    printf("Z\n");
}



// sets knob based on given values from another function
void set_knob(knob_t *knob, uint8_t bit_val){
  uint8_t int_val;
  
  int_val = bit_val;
  knob->knob_value = int_val;
    knob->control_value = int_val;
    knob->curr_ex = int_val;
  
    update_exes(knob);
    printf("test int %10d for controls\n", knob->control_value);
    printf("test int %10d for knob\n", knob->knob_value);
}

// reads values of knobs on the device and sets the knobs accordingly - used for new inicialization
void initialize_controls() {
    printf("Y\n");
    // update separate knob values
      printf("BLUE knob\n");
    uint8_t bit_val1 = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    set_knob(blue_knob, bit_val1);
      printf("GREEN knob\n");
    uint8_t bit_val2 = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 0x001);
    set_knob(green_knob, bit_val2);
      printf("RED knob\n");
    uint8_t bit_val3 = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 0x002);
    set_knob(red_knob, bit_val3);
  
    printf("Z\n");
}

// Currently unused function (was used for texting purposes)
void print_knobs() {
     rgb_knobs_value = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
     // read buttons
     buttons = rgb_knobs_value >> 24;

     /* Store the read value to the register controlling individual LEDs */
     *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = rgb_knobs_value;
}

// Function for reading if red button was pressed (returns true if pressed)
bool check_red(){
     int int_val;
     unsigned int uint_val;
     uint8_t mask = 1;

     uint8_t value = ((buttons >> 2) & mask);

     /* Assign value read from knobs to the basic signed and unsigned types */
     int_val = value;
     uint_val = value;

     /* Print values */
     printf("red int %10d uint 0x%08x\n", int_val, uint_val);
     if (value == 1) {
       return true;
     }
  return false;
}

// Function for reading if blue button was pressed (returns true if pressed)
bool check_blue(){
     int int_val;
     unsigned int uint_val;
     uint8_t mask = 1;

     uint8_t value = (buttons & mask);

     /* Assign value read from knobs to the basic signed and unsigned types */
     int_val = value;
     uint_val = value;

     /* Print values */
     printf("blue int %10d uint 0x%08x\n", int_val, uint_val);
     if (value == 1) {
       return true;
     }
  return false;
}









// ==== MAIN FUNCTION ====


int main(int argc, char *argv[])
{
  /* Serialize execution of applications */

  /* Try to acquire lock the first */
  if (serialize_lock(1) <= 0) {
    printf("System is occupied\n");

    if (1) {
      printf("Waitting\n");
      /* Wait till application holding lock releases it or exits */
      serialize_lock(0);
    }
  }


  
  printf("Hello world\n");

  sleep(1);

  /*
   * Memory mapping for pheriperals
   */
  mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

  /* If mapping fails exit with error code */
  if (mem_base == NULL) exit(1);



  parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  if (parlcd_mem_base == NULL)  exit(1);
  fb  = (unsigned short *)malloc(320*480*2);
  fbackground = (unsigned short *)malloc(320 * 480 * 2);
  font = &font_winFreeSystem14x16;

  // Turns off RGB LEDs and LED line
  *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0x000000;
  *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0x000000;
  *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = 0x000000;
  parlcd_hx8357_init(parlcd_mem_base);



  allocd = 0;
  freed = 0;

  // Initialize and allocate space for controls
  red_knob = initialize_knob();
  green_knob = initialize_knob();
  blue_knob = initialize_knob();

  // initialize ship
  ship = initialize_ship_t();

  draw_introduction();
  sleep(2);
  // print menu as default
  draw_menu();


  
  struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 30 * 1000 * 1000};
  int counter = 0;
  while(game_running) {
    //print_knobs();
    if (game_state == _MENU) {

      // check input
      read_controls();

      // if BLUE dial pressed -> game_state = _PLAYING
      if(check_blue()) {
        game_state = _PLAYING;
        draw_instructions();
        generate_ship();
        initialize_asteroids();
        generate_wave();
        for (i = 0; i<32; i++) {
          ship_hp = (ship_hp << 1) + 1;
        }
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = ship_hp;
        read_controls();
        initialize_controls();
      }

      // if RED dial pressed, end game -> game_running = false
      if(check_red()) {
        draw_bye();
        game_running = false;
      }
    } 
    
    
    else if (game_state == _PLAYING) {
      // read controls
      read_controls();

      // update gametick
      game_tick();

      // draw game
      draw_game();

      if (ship_hp <= 0) {
        // print menu
        draw_gameover();
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0xFF0401;
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0xFF0401;
        game_state = _GAMEOVER;
        sleep(1);
      }

      // if BLUE dial pressed -> game_state = _PAUSED
      if(check_blue()) {
        draw_paused();
        game_state = _PAUSED;
        sleep(1);
      }
    } 
    
    
    else if (game_state == _PAUSED) {
      // check input
      read_controls();

      // if BLUE dial pressed -> game_state = _PLAYING
      if(check_blue()) {
        game_state = _PLAYING;
        sleep(1);
      }

      // if RED dial pressed, exit to menu -> game_state = _MENU
      if(check_red()) {
        score = 0;
        ship_hp = 0;
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = ship_hp;
        free_asteroid_field();

        if(laser != NULL) {
          free(laser);
        }
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0x000000;
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0x000000;
        game_state = _MENU;
        // print menu
        draw_menu();
        sleep(1);
      }
    }

    else if (game_state == _GAMEOVER) {
      // check input
      read_controls();

      // if RED dial pressed, exit to menu -> game_state = _MENU
      if(check_red()) {
        // print menu
        score = 0;
        ship_hp = 0;
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = ship_hp;
        free_asteroid_field();

        if(laser != NULL) {
          free(laser);
        }
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0x000000;
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0x000000;
        draw_menu();
        game_state = _MENU;
        sleep(1);
      }
    }

    // Print the healthbar
      
    // set the delay between gameticks
    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    printf("gamestate %10d\n", game_state);
    printf("gamestate %10d\n", counter);
    printf("gamestate %10d\n", allocd);
    printf("gamestate %10d\n", freed);
    printf("SCORE : %10d\n", score);
    counter++;
    /*if (counter > 480) {
      game_running = false;
    }*/
  }



  printf("Goodbye world\n");

  // Release memory for altered knob values
  free(red_knob);
  free(green_knob);
  free(blue_knob);

  // Release memory for laser blasts
  if(laser != NULL) {
    free(laser);
  }

  // Release memory for asteroids
  free_asteroid_field();

  // Release memory for ship
  if(ship != NULL) {
    if(ship->cannon != NULL) {
      free(ship->cannon);
    }
    if(ship->ship_model != NULL) {
      free(ship->ship_model);
    }
    free(ship);
  }
  
  printf("All space stuff cleared\n");

  /* Release the lock */
  serialize_unlock();

  return 0;
}
